module.exports = function (io) {
    var lightsOn = [false, false, false, false, false];

    setInterval(function toggleLastLight() {
        var index = lightsOn.length - 1;
        var newState = lightsOn[index] = !lightsOn[index];
        io.emit('change', {
            id: index,
            isOn: newState
        });
    }, 1000);

    io.on('connect', function (socket) {
        const id = socket.id;
        console.log(id + ' connected');

        socket.emit('state', lightsOn);

        socket.on('toggle', function (data) {
            var index = Number(data.id);
            if (index < 0 || index >= lightsOn.length) {
                socket.emit('alert', 'Invalid light index');
                return;
            } else if (index == lightsOn.length - 1) {
                socket.emit('alert', 'Light can not be toggled');
                return;
            }
            var newState = lightsOn[index] = !lightsOn[index];
            io.emit('change', {
                id: index,
                isOn: newState,
                cause: socket.request.user.username
            });
        });

        socket.on('disconnect', function () {
            console.log(id + ' disconnected');
        });

    });

};