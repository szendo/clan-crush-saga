var express = require('express');
var session = require('express-session');
var app = express();
var server = require('http').createServer(app);

const SUPER_SECRET = 'potato-salad';

var MongoStore = require('connect-mongo')(session);
var store = new MongoStore({
    url: 'mongodb://localhost/test-app'
});

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
    function (username, password, done) {
        if (username !== password) {
            return done(null, false);
        }
        return done(null, {
            username: username
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.get('/', function (req, res) {
    res.redirect('/index.html');
});
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({
    extended: false
}));
app.use(session({
    resave: false,
    saveUninitialized: false,
    secret: SUPER_SECRET,
    store: store
}));
app.use(passport.initialize());
app.use(passport.session());

app.get('/index.html', function (req, res, next) {
    if (req.user) {
        console.log('user', req.user);
        next();
    } else {
        res.redirect('/login.html');
    }
});

app.use('/', express.static('web'));

app.post('/login', passport.authenticate('local', {
    successRedirect: '/index.html',
    failureRedirect: '/login.html'
}));

var io = require('socket.io')(server, {
    serveClient: false
});
var passportSocketIo = require("passport.socketio");
io.use(passportSocketIo.authorize({
    cookieParser: require('cookie-parser'),
    passport: passport,
    secret: SUPER_SECRET,
    store: store,
    success: function (data, accept) {
        accept(null, true);
    },
    fail: function (data, message, error, accept) {
        if (error) {
            throw new Error(message);
        }
        console.log('failed auth: ', message, data);
        accept(null, false);
    }
}));
var lightsModule = require('./lights');
lightsModule(io);

server.listen(3000);