document.addEventListener('DOMContentLoaded', function () {

    var stage = new createjs.Stage('wargame');

    var buttons = new createjs.Container();
    var gameScene = new createjs.Container();
    var statusText = new createjs.Text("Connecting...", "24px sans-serif").set({
        textAlign: "center",
        x: 400
    });
    var goldText = new createjs.Text("", "24px sans-serif").set({
        textAlign: "right",
        x: 796
    });

    stage.addChild(new createjs.Bitmap('/res/img/background.png'));
    stage.addChild(placeCastle(0));
    stage.addChild(placeCastle(1));

    stage.addChild(gameScene);
    stage.addChild(buttons);
    stage.addChild(statusText);
    stage.addChild(goldText);

    createjs.Ticker.timingMode = createjs.Ticker.RAF;
    createjs.Ticker.addEventListener('tick', stage);

    var entities = {};

    // https://github.com/Automattic/socket.io-client/issues/812
    var lobby = io(':3333/lobby');

    lobby.on('match', function (data) {

        var unitSpriteSheets = {};
        var buttonStates = {
            minerUp: 0, minerDown: 1
        };

        var units = data.units;

        units.forEach(function (u) {
            unitSpriteSheets[u.type] = new createjs.SpriteSheet(u.visuals.spriteData);
            for (var k in u.visuals.buttonStates) {
                if (u.visuals.buttonStates.hasOwnProperty(k)) {
                    buttonStates[u.type + k] = u.visuals.buttonStates[k];
                }
            }
        });

        var unitTypes = units.map(function (u) {
            return u.type;
        });

        var buttonSpriteSheet = new createjs.SpriteSheet({
            images: ['res/img/button.png'],
            frames: {
                width: 48,
                height: 48,
                regX: 24,
                regY: 24
            },
            animations: buttonStates
        });


        statusText.text = 'Connected!';

        var socket = io(':3333/games');
        socket.on('state', function (data) {
            data.units.forEach(function (unit) {
                placeEntity(unit, unitSpriteSheets[unit.type]);
            });
        });

        socket.on('state', function (data) {
            data.units.forEach((entityData) => {
                var w = placeEntity(entityData, unitSpriteSheets[entityData.type]);
                entities[entityData.id] = w;
                gameScene.addChild(w);
            });
        });

        socket.on('created', function (data) {
            var w = placeEntity(data, unitSpriteSheets[data.type]);
            entities[data.id] = w;
            gameScene.addChild(w);
        });

        socket.on('stat', onStatUpdate);

        socket.on('tick', function (data) {
            data.changed.forEach(function (unit) {
                var entity = entities[unit.id];

                if (unit.hasOwnProperty('state')) {
                    entity.gotoAndPlay(unit.state);
                }

                if (unit.hasOwnProperty('position')) {
                    moveEntity(entity, unit);
                }
            });

            data.deleted.forEach(function (id) {
                gameScene.removeChild(entities[id]);
                delete entities[id];
            });
        });

        socket.on('ended', function (endData) {
            var status = endData.status;
            statusText.text = status == 'won' ? 'You ₩'
                : status == 'lost' ? 'You lost'
                : 'It\'s a tie!';
            stage.removeChild(buttons);
            goldText.text = '';
        });

        ['miner'].concat(unitTypes).forEach(function (unit, i) {
            var buyUnit = new createjs.Sprite(buttonSpriteSheet);
            buyUnit.on('click', function () {
                socket.emit('buy', {type: unit}, function (response) {
                    if (response.status == 'ok') {
                        onStatUpdate(response.payload);
                    } else {
                        console.log(response);
                    }
                });
            });
            buyUnit.set({
                x: 24 + 48 * i,
                y: 24,
                scaleX: data.flipped ? -1 : 1
            });
            new createjs.ButtonHelper(buyUnit, unit + 'Up', unit + 'Up', unit + 'Down');
            buttons.addChild(buyUnit);
        });

        function onStatUpdate(stats) {
            statusText.text = "Health: " + stats.health;
            goldText.text = '$' + stats.gold;
        }

    });

    function placeEntity(unit, sprite) {
        var w = new createjs.Sprite(sprite, unit.state);
        w.set({
            x: transformRoadCoord(unit.position),
            y: 370,
            scaleX: unit.player === 0 ? 1 : -1
        });
        return w;
    }

    function moveEntity(entity, unit) {
        entity.set({
            x: transformRoadCoord(unit.position)
        })
    }

    function placeCastle(id) {
        var c = new createjs.Bitmap('/res/img/castle.png');
        c.set({
            regX: 238,
            regY: 340,
            x: transformRoadCoord(id === 0 ? 0 : 1000),
            y: 370,
            scaleX: id === 0 ? 1 : -1
        });
        return c;
    }

    function transformRoadCoord(pos) {
        return 704 * pos / 1000 + 48;
    }

});
