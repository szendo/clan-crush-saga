const config = require('./config.json').google_oauth2;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

module.exports.configurePassport = function (passport, entities) {
    passport.use(new GoogleStrategy({
        clientID: config.clientID,
        clientSecret: config.clientSecret,
        callbackURL: config.callbackURL
    }, (accessToken, refreshToken, profile, done) => {
        done(null, {
            id: profile.id,
            displayName: profile.displayName
        });
    }));
};

module.exports.configureExpress = function (app, passport, entities) {
    app.get('/google-login', passport.authorize('google', {
        scope: config.scope
    }));

    app.get('/process-google-login', passport.authorize('google', {
        failureRedirect: '/login',
        failureFlash: 'Login with Google account failed!'
    }), (req, res) => {
        // Check if there's a user already mapped to this Google Account
        entities.User.findOne({
            claims: {
                $elemMatch: {profileId: req.account.id, profileType: 'google'}
            }
        }, (err, user) => {
            if (user) {
                // If there is, log in with it
                req.login(user, (err) => {
                    if (!err) {
                        res.redirect('/');
                    } else {
                        console.log('Error while logging in external user', err);
                        res.redirect('/login');
                    }
                });
            } else {
                // If there is not, save the profile data in the session, then redirect to picking a username
                req.session.googleProfile = req.account;
                res.redirect('/google-finalize')
            }
        });
    });

    app.get('/google-finalize', (req, res) => {
        if (!req.session.googleProfile) {
            return res.redirect('/');
        }
        var error = req.flash('error');
        res.render('google-finalize', (error.length > 0) ? {
            displayName: req.session.googleProfile.displayName,
            error: error
        } : {displayName: req.session.googleProfile.displayName});
    });

    app.post('/process-google-finalize', (req, res) => {
        // If there's no profile data in the session, the user shouldn't be here
        if (!req.session.googleProfile) {
            return res.redirect('/');
        }
        // Count the users in the database with this name (aka is the username already taken)
        entities.User.count({username: req.body.username}, (err, count) => {
            if (count > 0) {
                req.flash('error', 'Username already exists!');
                return res.redirect('/google-finalize');
            } else {
                // If the username is free, create the user (without password; only external login is allowed)
                new entities.User({
                    username: req.body.username,
                    claims: [{profileId: req.session.googleProfile.id, profileType: 'google'}]
                }).save((err, user) => {
                    if (err) {
                        console.log('Error while creating user', err);
                        req.flash('error', 'Error while creating user!');
                        return res.redirect('/google-finalize');
                    } else {
                        // Log in with the newly created user
                        delete req.session.googleProfile;
                        req.login(user, (err) => {
                            if (!err) {
                                res.redirect('/');
                            } else {
                                console.log('Error while logging in new external user', err);
                                res.redirect('/login');
                            }
                        })
                    }
                });
            }
        });
    });
};
