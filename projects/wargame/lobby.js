const Match = require('./match');
const uid = require('uid-safe');
const units = require('./units');

module.exports = function (io) {

    var lobby = io.of('/lobby');
    var games = io.of('/games');

    var queue = [];
    var matches = [];

    lobby.on('connection', (socket) => {
        var user = socket.request.user;
        console.log(user.username + ' connected to lobby');

        var ongoingMatch = matches.find(m => m.user1.id == user.id || m.user2.id == user.id);
        if (typeof ongoingMatch !== 'undefined') {
            if (ongoingMatch.user1.id == user.id) {
                ongoingMatch.match.changePlayer1Id(socket.id);
                lobby.to(socket.id).emit('match', {
                    units: units.getUnitVisuals(),
                    flipped: false
                });
            } else {
                ongoingMatch.match.changePlayer2Id(socket.id);
                lobby.to(socket.id).emit('match', {
                    units: units.getUnitVisuals(),
                    flipped: true
                });
            }
        } else {
            var queueEntry = queue.find(e => e.user.id == user.id);
            if (typeof queueEntry !== 'undefined') {
                queueEntry.socketId = socket.id;
            } else {
                var partnerQueueEntry = findPartner(user);
                if (partnerQueueEntry != null) {
                    createMatch(partnerQueueEntry, {user: user, socketId: socket.id});
                } else {
                    queue.push({
                        user: user,
                        socketId: socket.id
                    });
                }
            }
        }

        socket.on('disconnect', () => {
            console.log(user.username + ' disconnected from lobby');
            var index = queue.findIndex(e => e.user.id == user.id);
            if (index != -1) {
                console.log(user.username + ' removed from queue');
                queue.splice(index, 1);
            }
        });

        function findPartner(user) {
            if (queue.length > 0) {
                return queue.splice(0, 1)[0];
            } else {
                return null;
            }
        }

        function createMatch(p1, p2) {
            uid(8, (err, gameId) => {
                console.log('Creating game: ' + gameId);
                matches.push({
                    gameId: gameId,
                    user1: p1.user,
                    user2: p2.user,
                    match: new Match(games, units.getUnitStats(), {
                        gameId: gameId,
                        player1Id: p1.socketId,
                        player2Id: p2.socketId
                    }, (stats) => {
                        require('./stats').sendStats({
                            players: [p1.user.id, p2.user.id],
                            winner: stats.winner,
                            playerStats: stats.playerStats
                        });

                        var index = matches.findIndex(m => m.gameId == gameId);
                        if (index != -1) {
                            matches.splice(index, 1);
                        }
                    })
                });
                lobby.to(p1.socketId).emit('match', {
                    units: units.getUnitVisuals(),
                    flipped: false
                });
                lobby.to(p2.socketId).emit('match', {
                    units: units.getUnitVisuals(),
                    flipped: true
                });
            });
        }

    });

};