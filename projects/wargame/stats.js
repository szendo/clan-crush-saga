var uid = require('uid-safe');

function Stats() {
    var childProcess = require('child_process').fork('./stats/app');

    var queryQueue = {};

    childProcess.on('message', (m) => {
        if (m.type == 'queryResponse') {
            if (queryQueue.hasOwnProperty(m.data.queryId)) {
                var callback = queryQueue[m.data.queryId];
                delete queryQueue[m.data.queryId];
                callback(m.data.result);
            }
        }
    });

    this.sendStats = function (stats) {
        childProcess.send({
            type: 'stats',
            data: stats
        });
    };

    this.queryStats = function (userId, callback) {
        uid(8, function (id) {
            queryQueue[id] = callback;
            childProcess.send({
                type: 'query',
                data: {
                    queryId: id,
                    userId: userId
                }
            });
        })
    }

}

module.exports = new Stats();