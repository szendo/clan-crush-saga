var Game = require('./game').Game;

module.exports = function (io, unitStats, data, endCallback) {
    var gameId = data.gameId;
    var p1 = data.player1Id, p2 = data.player2Id;
    var p1Ok = false, p2Ok = false;
    var game = new Game(unitStats);
    var started = false;

    this.changePlayer1Id = (id) => {
        p1 = id;
        p1Ok = false;
        io.on('connect', onReconnect);
    };

    this.changePlayer2Id = (id) => {
        p2 = id;
        p2Ok = false;
        io.on('connect', onReconnect);
    };

    io.on('connect', function onConnect(socket) {
        const id = socket.id;

        if (id !== p1 && id !== p2) {
            console.log(id + ' ignored in ' + gameId);
            return;
        }

        if (id === p1) {
            p1Ok = true;
        } else {
            p2Ok = true;
        }

        console.log(socket.request.user.username + ' connected to game ' + gameId);
        socket.join(gameId);

        socket.on('buy', (data, fn) => {
            fn(game.buyUnit(id === p1 ? 0 : id === p2 ? 1 : -1, data.type));
        });

        io.to(gameId).emit('state', game.getState());

        if (p1Ok && p2Ok && !started) {
            game.start();
            started = true;
            io.removeListener('connect', onConnect);
        }

    });

    function onReconnect(socket) {
        const id = socket.id;

        if (id !== p1 && id !== p2) {
            console.log(id + ' ignored in ' + gameId);
            return;
        }

        if (id === p1) {
            p1Ok = true;
        } else {
            p2Ok = true;
        }

        console.log(socket.request.user.username + ' reconnected to game ' + gameId);
        socket.join(gameId);

        socket.on('buy', (data, fn) => {
            fn(game.buyUnit(id === p1 ? 0 : id === p2 ? 1 : -1, data.type));
        });

        io.to(socket.id).emit('state', game.getState());

        if (p1Ok && p2Ok) {
            io.removeListener('connect', onReconnect);
        }
    }

    game.on('created', (targetId, newUnit) => {
        io.to(gameId).emit('created', newUnit);
    });

    game.on('tick', (targetId, tickData) => {
        io.to(gameId).emit('tick', tickData);
    });

    game.on('ended', (targetId, endData) => {
        var id = targetId == 0 ? p1
            : targetId == 1 ? p2 : null;
        if (id !== null) {
            io.to(id).emit('ended', endData);
            if (started) {
                started = false;
            }
        }
    });

    game.on('endStats', (stats) => {
        endCallback(stats);
    });

    game.on('stat', (targetId, statData) => {
        var id = targetId == 0 ? p1
            : targetId == 1 ? p2 : null;
        if (id !== null) {
            io.to(id).emit('stat', statData);
        }
    })

};