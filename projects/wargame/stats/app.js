var vm = require('vm');

require('./persistence')(function (mongoose, entities) {
    console.log('[stats] Inserting achievements');
    loadAchievements();
    console.log('[stats] Ready to process');

    function loadAchievements() {
        (function _loadInteral(achievements) {
            if (achievements.length > 0) {
                var a = achievements[0];
                entities.Achievement.update({name: a.name}, {$setOnInsert: a}, {upsert: true}, () => {
                    _loadInteral(achievements.slice(1));
                })
            }
        })(require('../conf/achievements.json').achievements);
    }

    function storeStatsForPlayer(stats, playerId) {
        var score = stats.winner == playerId ? 1 : stats.winner == -1 ? 0 : -1;
        entities.Stat.findOneAndUpdate({_id: stats.players[playerId]}, {
            $inc: {
                'matches.won': +(score > 0),
                'matches.lost': +(score < 0),
                'matches.tied': +(score == 0),
                'gold.generated': stats.playerStats[playerId].gold.generated,
                'gold.spent': stats.playerStats[playerId].gold.spent,
                'units.killed': stats.playerStats[playerId].units.killed,
                'units.bought': stats.playerStats[playerId].units.bought,
                'units.lost': stats.playerStats[playerId].units.lost
            },
            $push: {
                matchHistory: {
                    $each: [{
                        score: score,
                        gold: stats.playerStats[playerId].gold,
                        units: stats.playerStats[playerId].units
                    }],
                    $sort: {_id: 1},
                    $slice: -5
                }
            }
        }, {upsert: true, new: true}, (err, stats) => {
            entities.Achievement.find({_id: {$nin: (stats.achievements || [])}}, (err, achievements) => {
                var statsContext = stats.toObject();
                vm.createContext(statsContext);

                var achieved = [];
                achievements.forEach(a => {
                    var result = vm.runInContext(a.expr, statsContext);
                    console.log(a.name, result);
                    if (result) {
                        achieved.push(a._id);
                    }
                });

                entities.Stat.update({_id: stats._id}, {$pushAll: {achievements: achieved}}, () => {
                });
            });
        });

    }

    function processStats(stats) {
        storeStatsForPlayer(stats, 0);
        storeStatsForPlayer(stats, 1);
    }

    function processQuery(queryId, userId) {
        entities.Stat.findById(userId)
            .populate('achievements')
            .exec(function (err, stat) {
                process.send({
                    type: 'queryResponse',
                    data: {
                        queryId: queryId,
                        result: stat != null ? stat.toObject() : null
                    }
                })
            });
    }

    process.on('message', function (m) {
        if (m.type == 'stats') {
            processStats(m.data)
        } else if (m.type == 'query') {
            processQuery(m.data.queryId, m.data.userId);
        } else {
            console.log('[stats] Unknown message type: ' + m);
        }
    })

});