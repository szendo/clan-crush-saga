var mongoose = require('mongoose');

const config = require('../config.json').mongoose;

module.exports = function (callback) {
    mongoose.connect(config.stats.uri, config.stats.options);

    const statSchema = mongoose.Schema({
        matches: {
            won: Number,
            lost: Number,
            tied: Number
        },
        gold: {
            generated: Number,
            spent: Number
        },
        units: {
            killed: Number,
            bought: Number,
            lost: Number
        },
        matchHistory: [{
            score: Number,
            gold: {
                generated: Number,
                spent: Number
            },
            units: {
                killed: Number,
                bought: Number,
                lost: Number
            }
        }],
        achievements: [{type: mongoose.Schema.Types.ObjectId, ref: 'Achievement'}]
    });
    const achievementSchema = mongoose.Schema({
        name: String,
        expr: String
    });

    mongoose.connection.once('open', callback.bind(this, mongoose, {
        Stat: mongoose.model('Stat', statSchema),
        Achievement: mongoose.model('Achievement', achievementSchema)
    }));

    this.getSessionStore = function (session) {
        var MongoStore = require('connect-mongo')(session);
        return new MongoStore({
            mongooseConnection: mongoose.connection
        });
    };
};
