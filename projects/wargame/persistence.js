var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

const config = require('./config.json').mongoose;

module.exports = function (callback) {
    mongoose.connect(config.main.uri, config.main.options);

    const userSchema = mongoose.Schema({
        username: String,
        password: String,
        claims: [{
            profileId: String,
            profileType: String
        }]
    });
    userSchema.statics.create = function (username, password, callback) {
        var User = this;
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, (err, passwordHash) => {
                new User({
                    username: username,
                    password: passwordHash
                }).save(callback);
            });
        });
    };
    userSchema.methods.checkPassword = function (password, callback) {
        bcrypt.compare(password, this.password, callback);
    };

    mongoose.connection.once('open', callback.bind(this, mongoose, {
        User: mongoose.model('User', userSchema)
    }));

    this.getSessionStore = function (session) {
        var MongoStore = require('connect-mongo')(session);
        return new MongoStore({
            mongooseConnection: mongoose.connection
        });
    };
};
