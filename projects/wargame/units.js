var _ = require('lodash');

const unitsConf = require('./conf/units.json');

module.exports = {
    getUnitStats: () => {
        return _.mapValues(_.indexBy(unitsConf.units, 'type'), 'stats');
    },
    getUnitVisuals: () => {
        return _.map(unitsConf.units, function (u) {
            return {
                type: u.type,
                visuals: u.visuals
            };
        });
    }
};
