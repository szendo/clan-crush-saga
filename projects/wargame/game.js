var EventEmitter = require('events').EventEmitter;
var uid = require('uid-safe');
var util = require('util');
var _ = require('lodash');

function Game(unitTypes) {
    const LEVEL_WIDTH = 1000;
    const TIME_LIMIT = 10 * 60 * 5; // 5 minutes

    const self = this;

    const playerStats = [0, 1].map(id => ({
        gold: {
            generated: 0,
            spent: 0
        },
        units: {
            killed: 0,
            bought: 0,
            lost: 0
        }
    }));

    var state = 'waiting';

    function initPlayer() {
        return {
            health: 1000,
            gold: 100,
            minerCount: 0,
            internal: {
                ready: false,
                income: [5, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
        }
    }

    var players = [initPlayer(), initPlayer()];

    var elapsed = 0;
    var units = [];

    var intervalId = null;

    this.start = () => {
        if (state !== 'waiting') {
            throw "Game already started";
        } else {
            intervalId = setInterval(tick.bind(self), 100);
            state = 'started';
        }
    };

    this.getState = () => {
        return {units: units};
    };

    this.buyUnit = (playerId, unitType) => {
        if (playerId < 0 || playerId > players.length) {
            return {
                status: 'error',
                message: 'Invalid player id'
            };
        } else if (state !== 'started') {
            return {
                status: 'error',
                message: 'Game not running'
            };
        } else if (unitTypes.hasOwnProperty(unitType)) {
            var player = players[playerId];
            var unitProto = unitTypes[unitType];
            if (player.gold < unitProto.cost) {
                return {
                    status: 'error',
                    message: 'Not enough gold'
                };
            } else {
                player.gold -= unitProto.cost;
                var newUnit = {
                    id: uid.sync(8),
                    type: unitType,
                    player: playerId,
                    health: unitProto.maxHealth,
                    state: 'walk',
                    atkCounter: unitProto.atkSpeed,
                    position: (playerId == 0) ? 0 : LEVEL_WIDTH,
                    flipped: (playerId != 0),
                    shield: unitProto.maxHealth,
                    shieldTimeout: 20
                };
                units.push(newUnit);

                playerStats[playerId].gold.spent += unitProto.cost;
                playerStats[playerId].units.bought++;

                self.emit('created', null, newUnit);
                return {
                    status: 'ok',
                    payload: player
                };
            }
        } else if (unitType == 'miner') {
            const minerCost = 50;
            var player = players[playerId];
            if (player.gold < minerCost) {
                return {
                    status: 'error',
                    message: 'Not enough gold'
                };
            } else {
                player.gold -= minerCost;
                player.minerCount++;
                player.internal.income[elapsed % player.internal.income.length]++;
                return {
                    status: 'ok',
                    payload: {
                        health: player.health,
                        gold: player.gold,
                        minerCount: player.minerCount
                    }
                };
            }
        } else {
            return {
                status: 'error',
                message: 'Invalid unit type'
            };
        }
    };

    function tick() {
        elapsed++;

        players.forEach((player, playerId) => {
            var plusGold = 2 * (player.internal.income[elapsed % player.internal.income.length]);
            player.gold += plusGold;
            playerStats[playerId].gold.generated += plusGold;
        });

        var movePhaseResults = movePhase(units);
        var attackPhaseResults = attackPhase(units);
        var tickData = {
            changed: [].concat(movePhaseResults.changed, attackPhaseResults.changed),
            deleted: [].concat(movePhaseResults.deleted, attackPhaseResults.deleted)
        };

        self.emit('tick', null, tickData);
        players.forEach((player, i) => {
            self.emit('stat', i, {
                health: player.health,
                gold: player.gold,
                minerCount: player.minerCount
            });
        });

        var gameStatus = checkGameStatus();
        if (gameStatus.status !== 'running') {
            if (intervalId !== null) {
                clearInterval(intervalId);
                intervalId = null;
            }

            if (gameStatus.status == 'won') {
                players.forEach((p, i) => {
                    self.emit('ended', i, {
                        status: gameStatus.winner == i ? 'won' : 'lost'
                    });
                });
            } else {
                players.forEach((p, i) => {
                    self.emit('ended', i, {
                        status: 'tie'
                    });
                });
            }

            self.emit('endStats', {
                winner: (gameStatus.status == 'won' ? gameStatus.winner : -1),
                playerStats: playerStats
            });
        }

    }

    function movePhase(units) {
        var results = {
            changed: [],
            deleted: []
        };

        _.forEachRight(units, (unit) => {
            var unitProto = unitTypes[unit.type];

            if (unit.shieldTimeout > 0) {
                unit.shieldTimeout--;
            }

            if (!atEndOfRoad(unit) && getClosestEnemy(unit, units) === null) {
                unit.position += unitProto.movSpeed * (unit.flipped ? -1 : 1);
                if (unit.state != 'walk') {
                    unit.state = 'walk';
                    results.changed.push({
                        id: unit.id,
                        state: unit.state,
                        position: unit.position
                    });
                } else {
                    results.changed.push({
                        id: unit.id,
                        position: unit.position
                    });
                }
            }
        });
        return results;
    }

    function attackPhase(units) {
        var results = {
            changed: [],
            deleted: []
        };

        _.forEachRight(units, (unit) => {
            var unitProto = unitTypes[unit.type];

            var closestEnemy = getClosestEnemy(unit, units);
            if (closestEnemy !== null) {
                if (unit.state != 'attack') {
                    unit.state = 'attack';
                    results.changed.push({
                        id: unit.id,
                        state: unit.state
                    });
                }
                if (unit.atkCounter > 0) {
                    unit.atkCounter--;
                } else {
                    unit.atkCounter = unitProto.atkSpeed;
                    if (closestEnemy.shieldTimeout <= 0 || closestEnemy.shield <= 0) {
                        closestEnemy.health -= unitProto.atkDamage;
                    } else {
                        closestEnemy.shield -= unitProto.atkDamage;
                        if (closestEnemy.shield < 0) {
                            closestEnemy.health += unitProto.shield;
                            unitProto.shield = 0;
                            unitProto.shieldTimeout = 0;
                        }
                    }
                    if (closestEnemy.health > 0) {
                        results.changed.push({
                            id: closestEnemy.id,
                            health: closestEnemy.health
                        });
                    } else {
                        units.splice(units.indexOf(closestEnemy), 1);
                        results.deleted.push(closestEnemy.id);

                        playerStats[unit.player].units.killed++;
                        playerStats[closestEnemy.player].units.lost++;
                    }
                }
            } else if (atEndOfRoad(unit)) {
                if (unit.state != 'attack') {
                    unit.state = 'attack';
                    results.changed.push({
                        id: unit.id,
                        state: unit.state
                    });
                }
                if (unit.atkCounter > 0) {
                    unit.atkCounter--;
                } else {
                    unit.atkCounter = unitProto.atkSpeed;

                    var otherPlayer = players[1 - unit.player]; // TODO rewrite to be less hacky
                    if (otherPlayer.health > 0) {
                        otherPlayer.health -= unitProto.atkDamage;
                    }
                }
            }
        });

        return results;
    }

    function checkGameStatus() {
        var loserIndex = _.findIndex(players, (p) => {
            return p.health <= 0;
        });
        if (loserIndex != -1) {
            return {
                status: 'won',
                winner: 1 - loserIndex // TODO rewrite to be less hacky
            };
        } else if (elapsed > TIME_LIMIT) {
            var playersValue = _.map(players, (p, id) => {
                var ownUnits = _.filter(units, (u) => {
                    return u.playerId == id;
                });
                var ownUnitsValue = _.sum(_.map(ownUnits, (u) => {
                    return unitTypes[u.type].gold;
                }));
                return p.gold + ownUnitsValue;
            });
            var winner = (playersValue[0] > playersValue[1]) ? 0
                : (playersValue[1] > playersValue[0]) ? 1
                : -1;

            if (winner != -1) {
                return {
                    status: 'won',
                    winner: winner
                };
            } else {
                return {
                    status: 'tie'
                };
            }
        } else {
            return {
                status: 'running'
            };
        }
    }

    function distanceOf(u1, u2) {
        return Math.abs(u1.position - u2.position);
    }

    function getClosestEnemy(unit, units) {
        var unitProto = unitTypes[unit.type];

        var closest = null;
        for (var u of units) {
            var distance = distanceOf(unit, u);
            if (u.player != unit.player && distance < unitProto.atkRange) {
                if (closest === null || distance < distanceOf(unit, closest)) {
                    closest = u;
                }
            }
        }
        return closest;
    }

    function atEndOfRoad(unit) {
        var unitProto = unitTypes[unit.type];

        return (unit.flipped == false && (unit.position >= LEVEL_WIDTH - unitProto.atkRange)
        || unit.flipped == true && (unit.position <= unitProto.atkRange));
    }

}
util.inherits(Game, EventEmitter);

module.exports = {
    Game: Game
};
