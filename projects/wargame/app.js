var express = require('express');
var app = express();
var server = require('http').createServer(app);

// Session secret
const SESSION_SECRET = 'power-pill';

require('./persistence')(function (mongoose, entities) {
    var sessionStore = this.getSessionStore(require('express-session'));

    // Initialize PassportJS
    var passport = require('passport');

    require('./local-auth').configurePassport(passport, entities);
    require('./google-auth').configurePassport(passport, entities);

    passport.serializeUser((user, done) => done(null, user.id));
    passport.deserializeUser((userId, done) => entities.User.findById(userId, {username: 1}, done));


    // Set up routing
    app.set('view engine', 'jade');

    app.use(require('express-session')({
        resave: false,
        saveUninitialized: false,
        secret: SESSION_SECRET,
        store: sessionStore
    }));
    app.use(require('connect-flash')());
    app.use(passport.initialize());
    app.use(passport.session());

    app.get('/', (req, res) => {
        if (req.user) {
            res.render('index', {
                name: req.user.username
            });
        } else {
            res.redirect('/login');
        }
    });

    app.get('/stats', (req, res) => {
        if (req.user) {
            require('./stats').queryStats(req.user.id, function (stats) {
                res.render('stats', {
                    total: {
                        matches: stats.matches,
                        gold: stats.gold,
                        units: stats.units
                    },
                    recent: {
                        matches: {
                            won: stats.matchHistory.filter(m => m.score == 1).length,
                            tied: stats.matchHistory.filter(m => m.score == 0).length,
                            lost: stats.matchHistory.filter(m => m.score == -1).length
                        },
                        gold: {
                            generated: stats.matchHistory.map(m => m.gold.generated).reduce((a,b) => a+b),
                            spent: stats.matchHistory.map(m => m.gold.spent).reduce((a,b) => a+b)
                        },
                        units: {
                            bought: stats.matchHistory.map(m => m.units.bought).reduce((a,b) => a+b),
                            lost: stats.matchHistory.map(m => m.units.lost).reduce((a,b) => a+b),
                            killed: stats.matchHistory.map(m => m.units.killed).reduce((a,b) => a+b)
                        }
                    },
                    achievements: (stats.achievements || [])
                });
            })
        } else {
            res.redirect('/login');
        }
    });

    app.get('/game', (req, res) => {
        if (req.user) {
            res.render('game');
        } else {
            res.redirect('/login');
        }
    });

    app.use('/', express.static('web'));

    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });

    app.use(require('body-parser').urlencoded({
        extended: false
    }));

    require('./local-auth').configureExpress(app, passport, entities);
    require('./google-auth').configureExpress(app, passport, entities);

    // Initialize SocketIO
    var io = require('socket.io')(server, {
        serveClient: false
    });
    io.use(require("passport.socketio").authorize({
        passport: passport,
        secret: SESSION_SECRET,
        store: sessionStore
    }));

    require('./lobby')(io);

    // start up the stat processing process
    require('./stats');

    server.listen(3333);
});
