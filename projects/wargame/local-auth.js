var LocalStrategy = require('passport-local').Strategy;

module.exports.configurePassport = function (passport, entities) {
    passport.use(new LocalStrategy((username, password, done) => {
        entities.User.findOne({username: username}, (err, user) => {
            if (user != null) {
                user.checkPassword(password, (err, passwordMatches) => done(err, passwordMatches && user));
            } else {
                return done(null, false);
            }
        });
    }));
};

module.exports.configureExpress = function (app, passport, entities) {
    app.get('/login', (req, res) => res.render('login', req.flash()));

    app.post('/process-login', passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: 'Invalid username or password!'
    }));

    app.get('/register', (req, res) => res.render('register', req.flash()));

    app.post('/process-register', (req, res) => {
        entities.User.count({username: req.body.username}, (err, count) => {
            if (count > 0) {
                req.flash('error', 'Username already exists!');
                res.redirect('/register');
            } else {
                entities.User.create(req.body.username, req.body.password, (err, user) => {
                    if (!err) {
                        req.login(user, (err) => {
                            if (!err) {
                                res.redirect('/');
                            } else {
                                console.log('Error while logging in new user', err);
                                res.redirect('/login');
                            }
                        });
                    } else {
                        console.log('Error while creating user', err);
                        req.flash('error', 'Error while creating user!');
                        res.redirect('/register');
                    }
                });
            }
        });
    });
};